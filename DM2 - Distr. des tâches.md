# Tâches

## Exigences (environ 2h)

1. Jon: Respécification (update) des exigences (10min)
2. Alex: Correction des CU (2h)
3. Alex: Mise au propre des corrections dans Wiki (20min)

Due date: 6 nov (mardi soir)

## Analyse (environ 1h15)

1. Tous: Décisions brèves et unanimes en équipe (30min)
2. Jon: Rédaction du paragraphe (30min)
3. Tous: Révision du paragraphe (15min)

Due date: 7 nov (merc soir)

## Diagramme d’activités (1h)

1. Tous: Distribuer activités (10min)
2. Tous: Déssiner un brouillon de son sous-diagramme d'activités (10min)
3. Jon: Concaténer le tout au propre dans Papyrus (45min)

## Diagramme de classes (~1h30)

1. Produire une liste de classes dans un fichier txt sur Bitbucket (10min)
2. Alex: Produire diagramme de classes dans Papyrus (1h20)

## Diagramme de séquances (***optionel***): ~1h30

1. Produire un diag. de séq. à la main, scanner (1h30)

Due date: 8 nov (jeudi soir)

## Prototype: ~4h30

1. Tingjun: Écrire code et soumettre pour tests (2h)
2. Tingjun: Faire tests, upload code au Bitbucket (2h - 3h)
3. Tous: Réviser le code (1h)

## Rapport: ~2h

1. Concaténer tout dans Word au propre, puis transformer en pdf (1h)
2. Tingjun: Faire le Bitbucket (1h)
3. Tous: Révision finale

Due date: 11 nov (dim soir)
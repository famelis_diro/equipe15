# #GYM

Projet du cours IFT-2255, Génie logiciel à l'Université de Montréal

## Pour débuter

Projet en cours de développement. Nous travaillons encore sur les spécifications du logiciel à produire. Il n'est donc pas près pour la production. Vous pouvez toujours cloner le dépôt.

### Prérequis

1. Java (JDK, OpenJDK, ...)
2. Eclipse
3. Papyrus

Les diagrammes utilisent Papyrus dans Eclipse. Le prototype peut être exécuté dans une console et la librairie standard de Java devrait être suffisante.

```
java -jar prototype.jar [args]
```

### Installation

Cloner le dépôt et exécuter.

```
git clone https://jgraveline@bitbucket.org/famelis_diro/equipe15.git ./DM-IFT2255

cd DM-IFT2255

java -jar prototype.jar [args]
```

## Tests

Il n'y a pas de suite de tests pour l'instant.

## À propos de ce README.md

* Ce README.md est produit à partir d'un exemple de README sur [github] (https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)

## Contributions

Le projet est présentement privé.

## Auteurs

* Tingjun Cai
* Alexandre L'Écuyer
* Jonathan Graveline

## License

Voir professeur. Le projet est privé pour l'instant.

## Remerciements

Merci aux responsables du cours IFT-2255 de l'Université de Montréal pour leurs enseignements sur comment développer un logiciel:

* Michalis Famelis
* Maude Sabourin
* Thomas Schweizer

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

class Menu {
    private String name;
    private Map<String, MenuEntry> entries = new LinkedHashMap<>();

    @FunctionalInterface
    interface Callback {
        void run() throws Exception;
    }

    private static class Quit extends Exception {}
    static void quit() throws Quit {
        throw new Quit();
    }

    public static class MenuEntry {
        private final String description;
        private final Callback callback;
        MenuEntry(String description, Callback callback) {
            this.description = description;
            this.callback = callback;
        }
    }

    Menu() {
        this("Menu");
    }
    Menu(String name) {
        this.name = name;
    }

    Menu addQuitOption() {
        return addEntry("Q", "Quitter", Menu::quit);
    }

    Menu addEntry(String code, String description, Callback callback) {
        return addEntry(code, new MenuEntry(description, callback));
    }

    private Menu addEntry(String code, MenuEntry entry) {
        code = code.toUpperCase();
        if (entries.containsKey(code)) {
            throw new IllegalStateException("Cannot add loopUntilQuit entry. Code is duplicate.");
        }
        entries.put(code, entry);
        return this;
    }

    private void printOptions() {
        IO.println(name);
        IO.println();
        for (String code: entries.keySet()) {
            IO.println(" " + code + ". " + entries.get(code).description);
        }
    }

    // False if user wants to quit.
    private boolean prompt() {
        IO.flush();
        printOptions();
        String option = IO.readLine("> ", false).toUpperCase();
        MenuEntry entry = entries.get(option);

        if (entry == null) {
            System.err.println("Option invalide.\n");
            return prompt();
        }

        try {
            entry.callback.run();
        } catch (Quit ignored) {
            return false;
        } catch (Exception e) {
            IO.println(e.getMessage());
        }
        try {
            System.in.read();
        } catch (IOException ignored) {
        }
        return true;
    }

    void loopUntilQuit() {
        while (prompt()) {}
    }
}

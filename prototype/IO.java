import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Classe d’utilités pour des opérations I/O.
 * Utiliser ces opérations pour demander les informations de l’utilisateur
 * et recevoir les données.
 */
class IO {
    private static Scanner scanner = new Scanner(System.in);

    private IO() {}

    /**
     * Effacer tous les contenus de l’écran
     */
    static void flush() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    private static void print(String... value) {
        for (String v: value) {
            System.out.print(v);
        }
    }
    static void println() {
        println("");
    }
    static void println(String... value) {
        for (String v: value) {
            System.out.println(v);
        }
    }
    static void println(Object... value) {
        for (Object v: value) {
            System.out.println(v.toString());
        }
    }

    static int readInt(String prompt) {
        print(prompt);
        return scanner.nextInt();
    }

    private static double readDouble(String prompt) {
        print(prompt);
        return scanner.nextDouble();
    }

    static int readMoneyToCents(String prompt) {
        double dollars = readDouble(prompt);
        return (int) Math.floor(dollars * 100);
    }

    static boolean readBoolean(String prompt) {
        print(prompt + " (Y/N): ");
        return scanner.next().charAt(0) == 'Y';
    }

    static Date readDate(String prompt, String format) throws ParseException {
        print(prompt + " (" + format + "): ");
        var dateFormat = new SimpleDateFormat(format);
        return dateFormat.parse(scanner.nextLine());
    }

    static Heure readHeure(String prompt) throws ParseException {
        Date date = readDate(prompt, "HH:MM");
        return new Heure(date.getHours(), date.getMinutes());
    }

    static Recurrence readRecurrence(String prompt) {
        print(prompt);
        return Recurrence.fromConsole();
    }

    static char readChar(String prompt) {
        print(prompt);
        return scanner.next().charAt(0);
    }

    static String readLine(String prompt, boolean lineBreak) {
        System.out.print(prompt);
        if (lineBreak) System.out.println();
        return scanner.nextLine();
    }

    static String readLine(String prompt) {
        return readLine(prompt, false);
    }
}

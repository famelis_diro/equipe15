import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.Date;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

class Util {
    static <T> T find(Iterable<T> iterable, Predicate<T> predicate) {
        for (T item: iterable) {
            if (predicate.test(item)) {
                return item;
            }
        }
        return null;
    }

    static <T> void forEach(Iterable<T> iterable, BiConsumer<T, Integer> consumer) {
        int i = 0;
        for (T item: iterable) {
            consumer.accept(item, i++);
        }
    }

    static Date parseDate(String source) throws ParseException {
        var format = new SimpleDateFormat("yyyy-MM-dd");
        return format.parse(source);
    }

    static String serializeDayOfWeek(DayOfWeek dayOfWeek) {
        String[] dayNames = new DateFormatSymbols().getShortWeekdays();
        return dayNames[dayOfWeek.getValue() % 7 + 1];
    }
}

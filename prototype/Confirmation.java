import java.io.Serializable;
import java.util.Date;

public class Confirmation implements Serializable {
    public Date created;
    Date sessionDate;
    public int professional;
    public int member;
    public int codeService;
    public String comments;

    public Confirmation(Date sessionDate, int professional, int member, int codeService, String comments) {
        this.created = new Date();
        this.sessionDate = sessionDate;
        this.professional = professional;
        this.member = member;
        this.codeService = codeService;
        this.comments = comments;
    }
}

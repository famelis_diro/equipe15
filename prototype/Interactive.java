interface Interactive {
    /**
     * Une classe Interactive doit implémenter cette opération qui prend le
     * contrôle du terminal jusqu’à le moment où l’utilisateur veut
     * retourner au menu principal. Cette opération HALTE la fils.
     * @param db
     */
    void loopUntilQuit(Database db);
}

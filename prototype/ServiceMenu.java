public class ServiceMenu implements Interactive {
    private Service service;

    ServiceMenu(Service service) {
        this.service = service;
    }

    private void viewClients() {
        // Le système affiche une liste des adhérants à la séance et donne l'option "Retour".
        IO.println("Adhérants: ");

        Util.forEach(service.clients, (client, i) -> {
            IO.println(i + 1 + ". " + client.toString());
        });
    }


    private void inscription(Database db) {
        IO.println("Inscription d’un membre à une séance.");
        var newMember = db.verifyIdentity();

        var nextSessions = service.nextSessions(5);
        if (nextSessions.isEmpty()) {
            IO.println("Aucune séance n’est disponible.");
            return;
        }

        IO.println("Prochaines " + nextSessions.size() + " séances:");
        Util.forEach(nextSessions, (session, i) -> IO.println(i + ". " + session));
        int sessionChoice = IO.readInt("Sélectionner la séance désirée: ");
        var session = nextSessions.get(sessionChoice);
        var comments = IO.readLine("Commentaires (facultatif): ");

        session.subscribed.add(newMember);
        service.clients.add(newMember);
        Confirmation confirmation = new Confirmation(
           session.date,
           service.professional.uuid,
           newMember.uuid,
           service.serviceCode,
           comments
        );
        db.confirmations.add(confirmation);
    }


    public void loopUntilQuit(Database db) {
        IO.println(toString());

        new Menu("Menu du service " + service.serviceCode)
        .addEntry("A", "Inscription", () -> this.inscription(db))
        .addEntry("C", "Consulter les adhérants", this::viewClients)
        .addQuitOption()
        .loopUntilQuit();
    }
}

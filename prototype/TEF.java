import java.io.Serializable;
import java.util.ArrayList;

/**
 * Les vendredis à minuit, la procédure comptable principale est initiée au Centre des
 * Données #GYM. Cette procédure génère des enregistrements sur le disque sous forme de
 * données de transfert électronique de fond (TEF). Le fichier TEF contient, pour la semaine :
 * le nom du professionnel, le numéro du professionnel et la liste des services pour lesquels
 * il donne une séance ainsi que le nombre de membres inscrits. Le fichier TEF est ensuite
 * adressé au service comptable. Les systèmes informatiques des banques concernées vont plus tard
 * s'assurer que le compte bancaire de chaque professionnel a reçu le montant approprié. Vous
 * n'êtes responsables que de la création des fichiers TEF, pas de leur traitement.
 */
public class TEF implements Serializable {
    public String proName;
    public int proID;
    public ArrayList<Service> services;
    public int inscrits = 0;

    public TEF(Member member, Database db) {
        proName = member.getName();
        proID = member.uuid;

        for (var service: db.repository.getServices()) {
            if (service.professional == member) {
                services.add(service);
                inscrits += service.clients.size();
            }
        }
    }
}

import javax.xml.crypto.Data;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;

class Repository implements Serializable, Interactive {
    private Database database;
    private ArrayList<Service> services = new ArrayList<>();

    Repository(Database database) {
        this.database = database;
    }

    public void loopUntilQuit(Database db) {
        new Menu("Répertoire des Services")
        .addEntry("C", "Confirmer", this::confirm)
        .addEntry("A", "Accéder au service", this::view)
        .addEntry("L", "Lister les services", this::list)
        .addEntry("D", "Supprimer services", this::delete)
        .addEntry("N", "Nouveau service", this::newService)
        .addQuitOption()
        .loopUntilQuit();
    }

    private void delete() {
        int serviceCode = IO.readInt("Entrer le numéro de service: ");
        var service = Util.find(services, s -> s.serviceCode == serviceCode);

        if (service == null) {
            // 4.a
            System.err.println("Service non trouvé.");
            return;
        }

        services.remove(service);
    }

    private void confirm() throws RuntimeException {
        int serviceCode = IO.readInt("Entrer le numéro de service: ");
        var service = Util.find(services, s -> s.serviceCode == serviceCode);

        if (service == null) {
            // 4.a
            System.err.println("Service non trouvé.");
            return;
        }

        IO.println(service.toString());

        var session = service.getNextSession();
        if (session == null) {
            throw new RuntimeException("Aucune session de ce service n’est disponible.");
        } else {
            IO.println("Prochaine session:");
            IO.println(session);
        }

        new Menu()
        .addEntry("r", "Entrer le code du membre", () -> {
            Member member = database.verifyIdentity(true);
            if (session.subscribed.contains(member)) {
                IO.println("Validé!");
                service.confirm(database);
                Menu.quit();
            }
        })
        .addQuitOption()
        .loopUntilQuit();
    }

    private void newService() {
        IO.println("S’il s’agit d’un nouveau professionnel, merci de s’inscrire d’abord.");
        IO.println("Vous pouvez entrer -1 pour retourner au loopUntilQuit principal.");

        try {
            var start = IO.readDate("Date de début du service", "dd-MM-yyyy");
            var end = IO.readDate("Date de fin du service", "dd-MM-yyyy");
            var heure = IO.readHeure("Heure du service");
            var recurrence = IO.readRecurrence("Récurrence hebdomadaire du service");
            int capacity = IO.readInt("Capacité maximale (maximum 30 confirmations): ");
            int proID = IO.readInt("Numéro du professionnel (9 chiffres): ");

            var pro = database.findMember(proID);
            if (pro == null || !pro.isProfessional) {
                System.err.println("Numéro invalid.");
                return;
            }

            // int serviceCode = IO.readInt("Code du service (7 chiffres)");
            int price = IO.readMoneyToCents("Frais du service (jusqu'à 100.00$)");
            var comments = IO.readLine("Commentaires (100 caractères) (facultatif).");

            Service seance = new Service(database.createServiceUUID(), start, end, heure, recurrence, capacity, pro, price, comments);
            services.add(seance);
        } catch (ParseException e) {
            System.err.println("Invalid service data.");
            e.printStackTrace();
        }
    }

    private void list() {
        if (services.size() == 0) {
            IO.println("Aucun service n’est ajouté.");
        }

        for (Service service: services) {
            IO.println(service.toString());
        }
    }

    private void view() {
        int id = IO.readInt("Service ID: ");
        var service = Util.find(services, s -> s.serviceCode == id);

        if (service == null) {
            // 4.a. Le service n'existe pas. L'agent en avise le professionnel et le scénario prend fin.
            System.err.println("Le service n’existe pas.");
            return;
        }

        new ServiceMenu(service).loopUntilQuit(database);
    }


    public ArrayList<Service> getServices() {
        return services;
    }
}

import java.io.Serializable;

public class Heure implements Serializable {
    private int hour;
    private int minute;

    Heure(int hour, int minute) {
        setHour(hour);
        setMinute(minute);
    }

    int getHour() {
        return hour;
    }

    void setHour(int hour) {
        if (hour < 0 || hour >= 24) {
            throw new IndexOutOfBoundsException("Hour must be within [0, 24)");
        }
        this.hour = hour;
    }

    int getMinute() {
        return minute;
    }

    void setMinute(int minute) {
        if (minute < 0 || minute >= 60) {
            throw new IndexOutOfBoundsException("Minute must be within [0, 60)");
        }
        this.minute = minute;
    }

    public String toString() {
        String prefix = minute < 10 ? "0" : "";
        return getHour() + ":" + prefix + getMinute();
    }
}

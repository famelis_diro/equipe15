import java.io.*;
import java.text.ParseException;
import java.time.DayOfWeek;
import java.util.ArrayList;

public class Main {
    private static String filename = "database.dat";
    private Database database;

    private static Database readFile() throws IOException, ClassNotFoundException {
        var in = new ObjectInputStream(new FileInputStream(filename));
        return (Database) in.readObject();
    }

    private static void writeFile(Database database) throws IOException {
        var file = new FileOutputStream(filename);
        var out = new ObjectOutputStream(file);
        out.writeObject(database);
        out.close();
        file.close();
    }

    private Main(Database database) {
        this.database = database;
    }

    /**
     * [X] - Ajouter/mettre à jour/supprimer un membre
     * [x] - Ajouter/mettre à jour/supprimer un service
     * [ ] - Confirmation à une séance
     * [X] - Procédure comptable
     */

    private void listMembers() {
        if (database.members.isEmpty()) {
            IO.println("Aucun membre n’existe.");
        }
        for (var m: database.members) {
            IO.println(m.toString());
        }
    }

    private void addMember() {
        var lastName = IO.readLine("Votre nom: ");
        var firstName = IO.readLine("Votre prénom: ");
        var professional = IO.readBoolean("Est-il un professionnel ? ");
        Member m = new Member(database.createMemberUUID(), firstName, lastName, professional);
        database.members.add(m);
        IO.println(m.toString());
    }

    private void deleteMember() {
        Member member = database.verifyIdentity();
        database.members.remove(member);
        IO.println("Membre supprimé.");
    }

    private synchronized void updateBalance() {
        var member = database.verifyIdentityIgnoreBalance();
        var solde = "Solde actuel: $" + member.balance / 100;

        new Menu("Mettre à jour le solde de " + member.getName() + " (" + solde + ")")
        .addEntry("P", "Paiement", () -> {
            int diff = IO.readMoneyToCents("Montant: ");
            member.addBalance(-diff);
            Menu.quit();
        })
        .addEntry("A", "Achat", () -> {
            int diff = IO.readMoneyToCents("Montant: ");
            member.addBalance(diff);
            Menu.quit();
        })
        .addQuitOption()
        .loopUntilQuit();
    }

    private void save() {
        try {
            writeFile(database);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void verifyIdentity() {
        database.verifyIdentity();
    }

    private void mainMenu() {
        new Menu("Menu principal")
        .addEntry("A", "Membres: Ajouter", this::addMember)
        .addEntry("S", "Membres: Supprimer", this::deleteMember)
        .addEntry("L", "Membres: Liste", this::listMembers)
        .addEntry("M", "Membres: Mettre à jour le solde", this::updateBalance)
        .addEntry("V", "Vérification d’identité", this::verifyIdentity)
        .addEntry("P", "Répertoire des Services", () -> database.repository.loopUntilQuit(database))
        .addEntry("C", "Procédure comptable", this::createTEF)
        .addEntry("I", "Sauvegarder", this::save)
        .addQuitOption()
        .loopUntilQuit();
    }

    private void createTEF() {
        var list = new ArrayList<TEF>();
        for (var member: database.members) {
            if (!member.isProfessional) continue;
            list.add(new TEF(member, database));
        }

        try {
            var file = new FileOutputStream(filename);
            var out = new ObjectOutputStream(file);
            out.writeObject(list.toArray());
            out.close();
            file.close();
        } catch (IOException e) {
            System.err.println("Impossible d’écrire.");
        }
    }

    private Main useTestDatabase() throws ParseException {
        int pro1 = 6897821;
        database.members.clear();
        database.repository.getServices().clear();
        // 2147483647
        var john = new Member(6781890, "John", "Appleseed", false);
        var pro = new Member(pro1, "Monsieur", "Professional", true);
        john.addBalance(-100);
        pro.addBalance(-100);

        database.members.add(john);
        database.members.add(pro);
        database.repository.getServices().add(new Service(
          2147483647,
           Util.parseDate("2018-11-15"),
           Util.parseDate("2018-12-20"),
           new Heure(20, 0),
           new Recurrence().add(DayOfWeek.MONDAY).add(DayOfWeek.FRIDAY),
           30,
           database.findMember(pro1),
           10,
           ""
        ));
        return this;
    }


    public static void main(String[] args) throws ParseException {
        // Menu principal ici.
        IO.println("Bonjour !");

        Database database = null;

        /*
        try {
            if (!Files.exists(Paths.get(filename))) {
                IO.println("La base de donnée n’existe pas. Elle sera créée.");
                database = new Database();
                writeFile(database);
            } else {
                database = readFile();
            }
        } catch (IOException e) {
            System.err.println("Impossible de lire le fichier.");
            System.exit(1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }
        */
        database = new Database();

        new Main(database).useTestDatabase().mainMenu();
    }
}

import java.io.Serializable;

class Member implements Serializable {
    // Frais d’adhésion
    private static int price = 100;

    public enum Status { Valid, Invalid, Suspended }

    final int uuid;

    /**
     * http://www.yacoset.com/how-to-handle-currency-conversions
     * Toujours, toujours utiliser les cents (au lieu de dollar) comme le solde.
     */
    volatile int balance = price;

    private String firstName;
    private String lastName;
    final boolean isProfessional;

    Member(int uuid, String firstName, String lastName, boolean isProfessional) {
        this.uuid = uuid;
        this.firstName = firstName.trim();
        this.lastName = lastName.trim();
        this.isProfessional = isProfessional;
    }

    String getName() {
        return lastName + ", " + firstName;
    }

    synchronized void addBalance(int diff) {
        this.balance += diff;
    }

    @Override
    public String toString() {
        var res = getName() + " (" + uuid + ")";
        if (isProfessional) {
            res += " (P)";
        }
        return res;
    }

    Status getStatus() {
        if (balance > 0) {
            return Status.Suspended;
        } else {
            return Status.Valid;
        }
    }
}
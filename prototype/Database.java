import java.io.Serializable;
import java.util.ArrayList;

class Database implements Serializable {
    Repository repository = new Repository(this);
    ArrayList<Member> members = new ArrayList<>();
    ArrayList<Confirmation> confirmations = new ArrayList<>();

    int createMemberUUID() {
        int uuid = (int) Math.floor(Math.random() * 1e10);
        if (findMember(uuid) != null) return createMemberUUID();
        return uuid;
    }

    int createServiceUUID() {
        int uuid = (int) Math.floor(Math.random() * 1e10);
        if (Util.find(repository.getServices(), s -> s.serviceCode == uuid) != null) {
            return createServiceUUID();
        }
        return uuid;
    }

    Member verifyIdentity() throws RuntimeException {
        return verifyIdentity(false);
    }

    /**
     * Vérifie un membre par I/O. S’il ne possède pas de statut valide,
     * une exception sera declenchée.
     */
    Member verifyIdentity(boolean silent) throws RuntimeException {
        int uuid = IO.readInt("Numéro du membre: ");
        Member member = findMember(uuid);
        Member.Status status = member == null ? Member.Status.Invalid : member.getStatus();
        if (status == Member.Status.Invalid) {
            throw new RuntimeException("Membre invalid.");
        } else if (status == Member.Status.Suspended) {
            throw new RuntimeException("Membre suspendu.");
        } else if (status == Member.Status.Valid) {
            if (!silent) IO.println("Membre valid.");
            return member;
        }
        return null;
    }

    /**
     * Vérifie un membre par I/O mais aucun exception ne sera declenchée.
     */
    Member verifyIdentityIgnoreBalance() {
        int uuid = IO.readInt("Numéro du membre: ");
        Member member = findMember(uuid);
        Member.Status status = member == null ? Member.Status.Invalid : member.getStatus();
        if (status == Member.Status.Invalid) {
            IO.println("Membre invalid.");
        } else {
            IO.println("Membre valid.");
            return member;
        }
        return null;
    }

    /**
     * Trouver un membre par UUID.
     */
    Member findMember(int uuid) {
        return Util.find(members, m -> m.uuid == uuid);
    }
}

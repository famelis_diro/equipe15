import java.io.Serializable;
import java.time.DayOfWeek;
import java.util.EnumSet;
import java.util.Iterator;

/**
 * Classe de donnée pour stocker la fréquence d’une activité par sameine.
 */
public class Recurrence implements Serializable {
    private EnumSet<DayOfWeek> set = EnumSet.noneOf(DayOfWeek.class);

    static Recurrence fromConsole() {
        var rec = new Recurrence();
        for (var d: DayOfWeek.values()) {
            var dayName = Util.serializeDayOfWeek(d);
            rec.set(d, IO.readBoolean(dayName));
        }
        return rec;
    }

    public boolean has(DayOfWeek dayOfWeek) {
        return set.contains(dayOfWeek);
    }

    private void set(DayOfWeek dayOfWeek, boolean occurs) {
        if (occurs) {
            set.add(dayOfWeek);
        } else {
            set.remove(dayOfWeek);
        }
    }

    public Recurrence add(DayOfWeek dayOfWeek) {
        set(dayOfWeek, true);
        return this;
    }

    int size() {
        return set.size();
    }

    @Override
    public String toString() {
        var b = new StringBuilder();
        for (var d: DayOfWeek.values()) {
            if (set.contains(d)) {
                b.append(Util.serializeDayOfWeek(d));
                b.append(", ");
            }
        }
        b.setLength(b.length() - 2);
        return b.toString();
    }
}

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

public class Service implements Serializable {
    /** Adhérants */
    final ArrayList<Member> clients = new ArrayList<>();

    /** Séances (cache) */
    final ArrayList<Session> sessions = new ArrayList<>();

    /** Code du service (7 chiffres) */
    final int serviceCode;

    Service(int serviceCode, Date start, Date end, Heure hour, Recurrence recurrence, int maximalCapacity, Member professional, int price, String comments) {
        this.serviceCode = serviceCode;
        this.created = new Date();
        this.start = start;
        this.end = end;
        this.hour = hour;
        this.recurrence = recurrence;
        this.maximalCapacity = maximalCapacity;
        this.professional = professional;
        this.price = price;
        this.comments = comments;
        createSessions();
    }

    /** Date de création du service. */
    private final Date created;

    /** Date de début du service (AA/MM/JJ): */
    private Date start;

    /** Date de fin du service (AA/MM/JJ): */
    private Date end;

    /** Heure du service (même heure à chaques séances) (HH:MM): */
    private Heure hour;

    /** Recurrence (quels jours il est offert à la même heure) */
    private Recurrence recurrence;

    /** Capacité maximale (maximum 30 confirmations) */
    private int maximalCapacity;

    /** Professionnel */
    Member professional;

    /** Frais du service (jusqu’à 100.00$) */
    int price;

    /** Commentaires (100 caractères) (facultatif). */
    String comments;

    @SuppressWarnings("deprecation")
    private void createSessions() {
        var date = start;
        for (int i = 0; date.before(end) || date.equals(end); i++) {
            var c = Calendar.getInstance();
            c.setTime(start);
            c.add(Calendar.WEEK_OF_MONTH, i / recurrence.size());
            c.add(Calendar.DAY_OF_WEEK, i % recurrence.size());
            date = c.getTime();
            date.setHours(hour.getHour());
            date.setMinutes(hour.getMinute());
            sessions.add(new Session(date));
        }
    }

    @Override
    public String toString() {
        final var b = new StringBuilder();
        final var date = new SimpleDateFormat("yyyy/MM/dd");

        b.append("Service (").append(serviceCode).append(")");
        b.append("\n\nProfessionnels: ").append(professional.toString());
        b.append("\nDate de début (AA/MM/JJ): ").append(date.format(start));
        b.append("\nDate de fin (AA/MM/JJ): ").append(date.format(end));
        b.append("\nHeure (HH:MM): ").append(hour.toString());
        b.append("\nRécurrence hebdomadaire: ").append(recurrence.toString());
        b.append("\nCapacité maximale: ").append(maximalCapacity);
        if (comments.length() > 0) {
            b.append("\nCommentaires: ").append(comments);
        }
        return b.toString();
    }

    Session getNextSession() {
        return Util.find(sessions, s -> s.date.after(new Date()));
    }

    ArrayList<Session> nextSessions(int count) {
        var first = getNextSession();
        if (first == null) return new ArrayList<>();

        var res = new ArrayList<Session>();
        res.add(first);
        var startIndex = sessions.indexOf(first);

        for (int i = startIndex, j = 0; i < sessions.size() && j < count; i++, j++) {
            res.add(sessions.get(i));
        }
        return res;
    }

    void confirm(Database db) {
        var newMember = db.verifyIdentity();
        clients.add(newMember);
        var confirmation = new Confirmation(
            new Date(), professional.uuid, newMember.uuid, serviceCode, comments
        );
        db.confirmations.add(confirmation);
    }

    static class Session implements Serializable {
        final Date date;
        ArrayList<Member> subscribed = new ArrayList<>();

        Session(Date date) {
            this.date = date;
        }

        @Override
        public String toString() {
            var format = SimpleDateFormat.getDateTimeInstance();
            return format.format(date);
        }
    }
}

# Glossaire pour le DM1 de génie logiciel.

[Retour au Devoir 1](Exigence)

Nous présentons ici les termes reliés au domaine du client pour qui nous développons le système « Centre de données ». Certains mots ont étés ajoutés parce qu’ils sont utilisés dans la description des cas d’utilisation.

## Voici la liste des termes:

1. **Gym**: Club centré autour de la santé physique.

2. **Service**: Une ou plusieurs séances d’activités offertes par un professionnel. Ces activités tournent autour de la santé physique puisque c’est un gym.

3. **Séance**: Une plage horaire précise où un service a lieu.

4. **Membre**: Un client du gym. Il peut accéder au gym et s’inscrire à des services.

5. **Agent**: Un employé du gym à la réception qui interagit avec le client et le système que nous allons produire.

6. **Professionnel**: Professeur, animateur ou expert qui offre un service au gym.

7. **Client**: Une personne faisant des requêtes à la réception.

8. **Adhérent**: Un client qui veut s’inscrire au gym, soit pour être un membre, un professionnel, ou bien un agent.

9. **Inscription**: Création d'un professionnel, d’un membre, d’un employé ou d’une adhérent à un service dans le centre de données. Cela lui permettra d’accéder au gym et/ou à un service.

10. **Dossier client**: Objet crée lors de l’inscription d'une personne contenant ses séances, son statut, la validité de son abonnement et ses données personnelles; nom, prénom, date de naissance, numéro de téléphone, adresse de résidence.

11. **Dictionnaire**: Structure de données contenant des informations reliées à un identifiant unique.

12. **Numéro**: Identifiant unique d’un membre, d’un employé ou d’un professionnel servant de clé dans les dictionnaires du centre de données.

13. **Code de service**: Identifiant unique d’un service servant de clé dans le centre de données.

14. **Service**: Une ou plusieurs séances d’activités offertes par un professionnel. Ces activités tournent autour de la santé physique.

15. **Séance**: Une plage horaire précise où un service a lieu.

16. **Centre de données**: Un système auquel on peut accéder pour enregistrer des données et les consulter.

17. **Réception**: Accueil du gym où l’on peut faire des requêtes pour accéder au gym et à des services.

18. **Suspension**: État d’un membre qui ne lui permet plus d’accéder au gym malgré sa présence dans le centre de données.

[Retour au Devoir 1](Devoir 1)